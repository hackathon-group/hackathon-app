import firebase from 'firebase';
import _ from 'lodash';

export class FirebaseDAO {
    app: any;
    database: any;

    constructor() {
        if (firebase.apps.length > 0)
            this.app = firebase.app();
        else {
            this.app = firebase.initializeApp({
                apiKey: "AIzaSyDFu5_EdKfg6KPVLMDiAGxMgm1A7lp9FlU",
                authDomain: "hackathon-4faf8.firebaseapp.com",
                databaseURL: "https://hackathon-4faf8.firebaseio.com",
                projectId: "hackathon-4faf8",
                storageBucket: "hackathon-4faf8.appspot.com",
                messagingSenderId: "62737571191"
            });
        }
    }

    public get(name: string) {
        let appAux = this.app;
        return new Promise(function(resolve) {
            firebase.database(appAux).ref(name).on("value", function(snapshot) {
                let list = [];
                snapshot.forEach( snap => {
                    list.push(snap.val());
                  });
                resolve(list);
            });
        });
    }

    public getSocket(name: string,ls: any[]) {
        let appAux = this.app;
        let list = ls;
        firebase.database(appAux).ref(name).limitToLast(1).on("child_added", function(snapshot) {
           // list.push(snapshot.val());
            list.push(snapshot.val())
        });
        return list;
    }

    public insert(name: string, postData: object) {
        let appAux = this.app;
        return new Promise(function(resolve) {
            //resolve(firebase.database(appAux).ref(name).push().set(postData));
            resolve(firebase.database(appAux).ref(name).push(postData));
        });
    }
}