import { FirebaseDAO } from '../../../providers';

export class Chat {

    name: string;
    app: any;

    constructor() {
        this.name = "chat";
    }

    public get() {
        return new FirebaseDAO().get(this.name);
    }

    public getSocket(ls: any[]) {
        return new FirebaseDAO().getSocket(this.name,ls);
    }
    
    public insert(model) {
        return new FirebaseDAO().insert(this.name, model);
    }

}