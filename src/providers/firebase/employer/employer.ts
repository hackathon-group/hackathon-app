import { FirebaseDAO } from '../../../providers';

export class Employer {

    name: string;
    app: any;

    constructor() {
        this.name = "employer";
    }

    public get() {
        return new FirebaseDAO().get(this.name);
    }
}