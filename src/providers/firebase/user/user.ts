import { FirebaseDAO } from '../../../providers';

export class User {

    name: string;
    app: any;

    constructor() {
        this.name = "user";
    }

    public get() {
        return new FirebaseDAO().get(this.name);
    }

    public insert(model) {
        return new FirebaseDAO().insert(this.name, model);
    }
}