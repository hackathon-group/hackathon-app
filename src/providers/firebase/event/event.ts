import { FirebaseDAO } from '../../../providers';

export class Event {

    name: string;
    app: any;

    constructor() {
        this.name = "event";
    }

    public get() {
        return new FirebaseDAO().get(this.name);
    }
}