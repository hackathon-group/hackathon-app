import { FirebaseDAO } from '../../../providers';

export class Faq {

    name: string;
    app: any;

    constructor() {
        this.name = "faq";
    }

    public get() {
        return new FirebaseDAO().get(this.name);
    }
}