import { FirebaseDAO } from '../../../providers';

export class Notification {

    name: string;
    app: any;

    constructor() {
        this.name = "notification";
    }

    public get() {
        return new FirebaseDAO().get(this.name);
    }
}