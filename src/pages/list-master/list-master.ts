import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, LoadingController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items, Event } from '../../providers';

import moment from 'moment';
import _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems;
  app: any;
  loading: any;

  

  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, public loadingCtrl: LoadingController) {
    this.presentLoadingDefault();
    new Event().get().then((data) => {

      let previsionDate = [];
      let finishDate = [];
      for (let item in data) {
        data[item].createAt = moment(Date.parse(data[item].createAt)).locale('pt-br');
        data[item].previsionAt = moment(Date.parse(data[item].previsionAt)).locale('pt-br');

        if (data[item].finishAt) {
          data[item].finishAt = moment(Date.parse(data[item].finishAt)).locale('pt-br');
          data[item].showAt = data[item].finishAt; 
          finishDate.push(data[item]);
        } else {
          data[item].showAt = data[item].previsionAt; 
          previsionDate.push(data[item]);
        }
      }

      previsionDate = _.orderBy(previsionDate, ['previsionAt'], ['desc']);
      finishDate = _.orderBy(finishDate, ['finishAt'], ['desc']);

      this.currentItems = _.concat(previsionDate, finishDate);

      console.log(this.currentItems);

      this.loading.dismiss();
    });
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Carregando...'
    });
    this.loading.present();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    // let addModal = this.modalCtrl.create('ItemCreatePage');
    // addModal.onDidDismiss(item => {
    //   if (item) {
    //     this.items.add(item);
    //   }
    // })
    // addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items.delete(item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }
}
