import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { NotificationPage } from './notification';
import { ModalPageModule } from '../modal/modal.module';

@NgModule({
  declarations: [
    NotificationPage
  ],
  imports: [
    ModalPageModule,
    IonicPageModule.forChild(NotificationPage),
    TranslateModule.forChild()
  ],
  exports: [
    NotificationPage
  ]
})
export class NotificationPageModule { }
