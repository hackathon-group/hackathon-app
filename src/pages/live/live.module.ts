import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { LivePage } from './live';

@NgModule({
  declarations: [
    LivePage,
  ],
  imports: [
    IonicPageModule.forChild(LivePage),
    TranslateModule.forChild()
  ],
  exports: [
    LivePage
  ]
})
export class LivePageModule { }
