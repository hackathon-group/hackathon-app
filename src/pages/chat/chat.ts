import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, LoadingController } from 'ionic-angular';

import { Items } from '../../providers';
import { Chat } from '../../providers';
import { AlertController } from 'ionic-angular';
import _ from 'lodash';

@IonicPage()
@Component({
  selector: 'chat-master',
  templateUrl: 'chat.html'
})
export class ChatPage {
  objectKeys = Object.keys;
  //currentItems: any[];
  app: any;
  loading: any;
  newMessage;

  currentItems = [
      {
        "createAt" : "11/06/2018 11:30",
        "message" : "Vi aqui no aplicativo que a torneira foi instalada. Gostei bastante da foto, vou passar a noite para ver",
        "user" : {
          "function" : "Cliente",
          "name" : "O Cliente",
          "thumb" : "https://ep01.epimg.net/brasil/imagenes/2017/05/27/deportes/1495915411_841618_1495917627_sumario_normal.jpg"
        }
      },
      {
        "createAt" : "11/06/2018 11:31",
        "message" : "Ficamos felizes que gostou. Que bom você estar acompanhando sua reforma pelo aplicativo.",
        "user" : {
          "function" : "Supervisor",
          "name" : "Sergio Fiorotti",
          "thumb" : "https://pbs.twimg.com/profile_images/574135576875507713/MFoBU8e7.jpeg"
        }
      },
      {
        "createAt" : "11/06/2018 15:00",
        "message" : "Esta atrasado, algo esta acontecendo?",
        "user" : {
          "function" : "Cliente",
          "name" : "O Cliente",
          "thumb" : "https://ep01.epimg.net/brasil/imagenes/2017/05/27/deportes/1495915411_841618_1495917627_sumario_normal.jpg"
        }
      },
      {
        "createAt" : "11/06/2018 16:31",
        "message" : "Encontramos alguns transtorno na instalação do azulejo. Fique tranquilo, não terá impacto na data da entrega",
        "user" : {
          "function" : "Supervisor",
          "name" : "Sergio Fiorotti",
          "thumb" : "https://pbs.twimg.com/profile_images/574135576875507713/MFoBU8e7.jpeg"
        }
      },
      {
        "createAt" : "11/06/2018 15:00",
        "message" : "Obrigado, precisando de ajuda me ligue",
        "user" : {
          "function" : "Cliente",
          "name" : "O Cliente",
          "thumb" : "https://ep01.epimg.net/brasil/imagenes/2017/05/27/deportes/1495915411_841618_1495917627_sumario_normal.jpg"
        }
      },
      {
        "createAt" : "12/06/2018 09:00",
        "message" : "Amanhã pela manhã vou estar no apartamento. Você estará lá?",
        "user" : {
          "function" : "Cliente",
          "name" : "O Cliente",
          "thumb" : "https://ep01.epimg.net/brasil/imagenes/2017/05/27/deportes/1495915411_841618_1495917627_sumario_normal.jpg"
        }
      },
      {
        "createAt" : "11/06/2018 16:31",
        "message" : "Sim, das 9:00 às 15:00",
        "user" : {
          "function" : "Supervisor",
          "name" : "Sergio Fiorotti",
          "thumb" : "https://pbs.twimg.com/profile_images/574135576875507713/MFoBU8e7.jpeg"
        }
      }
    
  ]

  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private alertCtrl: AlertController) {
    this.load();
  }

  load() {
    const chatClass = new Chat();
    this.presentLoadingDefault();

    this.currentItems = chatClass.getSocket(this.currentItems);

    this.loading.dismiss();
    /*new Chat().get().then((data) => {
      
      console.log(data);
      this.currentItems = data;
      this.loading.dismiss();
    });*/
  }

  presentLoadingDefault() {
    this.loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Carregando...'
    });
    this.loading.present();
  }

  ionViewDidLoad() {
  }


  addItem() {
    /*
    let alert = this.alertCtrl.create({
      title: 'Manutenção',
      subTitle: 'Estamos em processo de manutenção. Tente novamente mais tarde.',
      buttons: ['OK']
    });
    alert.present();
    return;*/

    if (this.newMessage) {
      new Chat().insert({
        "message": this.newMessage,
        "createAt": "11/06/2018 16:31",
        "user": {
          "function": "Cliente",
          "name": "O Cliente",
          "thumb": "https://ep01.epimg.net/brasil/imagenes/2017/05/27/deportes/1495915411_841618_1495917627_sumario_normal.jpg"
        }
      }).then(() => {
        this.newMessage = "";
        //this.load();
      });
    }
    
  }
}
