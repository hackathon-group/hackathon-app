import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { MainPage } from '../';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: 'test@example.com',
    password: 'test'
  };

  // Our translated text strings
  constructor(public navCtrl: NavController) {
  }

  // Attempt to login in through our User service
  doLogin() {
    this.navCtrl.push(MainPage);
  }

  seePassword () {
    var input = document.querySelector('#password input');
    var icon = document.querySelector('.field-login-password');
    if (input.getAttribute('type') == 'password') {
      input.setAttribute('type', 'text');
      icon.classList.add('password-visible');
    }
    else {
      input.setAttribute('type', 'password');
      icon.classList.remove('password-visible');
    }
  }
}
