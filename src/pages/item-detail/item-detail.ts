import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Items } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {
  item: any;
  statusColor: string;

  constructor(public navCtrl: NavController, navParams: NavParams, items: Items) {
    this.item = navParams.get('item') || items.defaultItem;
    switch(this.item.status) {
      case 'em andamento': this.statusColor = 'gray'; break;
      case 'finalizado com atraso': this.statusColor = 'orange'; break;
      case 'finalizado': this.statusColor = 'blue'; break;
    }
  }

  chat()
  {
    this.navCtrl.push('ChatPage');
  }

}
